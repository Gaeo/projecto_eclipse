import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;

public class Base {

	private JFrame frame;
	private JTextField num2;
	private JTextField num1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Base window = new Base();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Base() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(500, 100, 261, 369);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		editorPane.setBounds(10, 11, 225, 98);
		frame.getContentPane().add(editorPane);
		
		num2 = new JTextField();
		num2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		num2.setBorder(null);
		num2.setBounds(20, 58, 200, 44);
		frame.getContentPane().add(num2);
		num2.setColumns(10);
		
		num1 = new JTextField();
		num1.setBorder(null);
		num1.setBounds(120, 24, 100, 23);
		frame.getContentPane().add(num1);
		num1.setColumns(10);
		
		JButton btnNewButton = new JButton("9");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+9);
				
				
			}
		});
		btnNewButton.setBounds(10, 134, 45, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("6");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+6);
			}
		});
		btnNewButton_1.setBounds(10, 168, 45, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("3");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+3);
			}
		});
		btnNewButton_2.setBounds(10, 202, 45, 23);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("8");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+8);
			}
		});
		btnNewButton_3.setBounds(65, 134, 45, 23);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_5 = new JButton("5");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+5);
			}
		});
		btnNewButton_5.setBounds(65, 168, 45, 23);
		frame.getContentPane().add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("2");
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+2);
			}
		});
		btnNewButton_6.setBounds(65, 202, 45, 23);
		frame.getContentPane().add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("0");
		btnNewButton_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+0);
			}
		});
		btnNewButton_7.setBounds(10, 236, 100, 23);
		frame.getContentPane().add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("7");
		btnNewButton_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+7);
			}
		});
		btnNewButton_8.setBounds(120, 134, 45, 23);
		frame.getContentPane().add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("4");
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+4);
			}
		});
		btnNewButton_9.setBounds(120, 168, 45, 23);
		frame.getContentPane().add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("1");
		btnNewButton_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num2.setText(num2.getText()+1);
			}
		});
		btnNewButton_10.setBounds(120, 202, 45, 23);
		frame.getContentPane().add(btnNewButton_10);
		
		JButton btnNewButton_11 = new JButton("=");
		btnNewButton_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int one = Integer.parseInt(num1.getText());
				int two = Integer.parseInt(num2.getText());
				
				if (actionRecived.getText().equals("sub")) {
					int sub = one - two;
					num2.setText(String.valueOf(sub));
				}
				
				if (actionRecived.getText().equals("add")) {
					int add = one + two;
					num2.setText(String.valueOf(add));
				}
				
				if (actionRecived.getText().equals("div")) {
					int div = one / two;
					num2.setText(String.valueOf(div));
				}
				
				if (actionRecived.getText().equals("mul")) {
					int mul = one * two;
					num2.setText(String.valueOf(mul));
				}
			}
		});
		btnNewButton_11.setBounds(120, 236, 45, 23);
		frame.getContentPane().add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("-");
		btnNewButton_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1.setText(num2.getText());
				actionRecived.setText("sub");
				num2.setText(null);
			}
		});
		btnNewButton_12.setBounds(175, 134, 45, 23);
		frame.getContentPane().add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton("+");
		btnNewButton_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1.setText(num2.getText());
				actionRecived.setText("add");
				num2.setText(null);
			}
		});
		btnNewButton_13.setBounds(175, 168, 45, 23);
		frame.getContentPane().add(btnNewButton_13);
		
		JButton btnNewButton_14 = new JButton("/");
		btnNewButton_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1.setText(num2.getText());
				actionRecived.setText("div");
				num2.setText(null);
			}
		});
		btnNewButton_14.setBounds(175, 202, 45, 23);
		frame.getContentPane().add(btnNewButton_14);
		
		JButton btnNewButton_15 = new JButton("*");
		btnNewButton_15.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				num1.setText(num2.getText());
				actionRecived.setText("mul");
				num2.setText(null);
			}
		});
		btnNewButton_15.setBounds(175, 236, 45, 23);
		frame.getContentPane().add(btnNewButton_15);
		
		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				num1.setText(null);
				num2.setText(null);
				actionRecived.setText(null);
			}
		});
		btnC.setBounds(130, 273, 89, 23);
		frame.getContentPane().add(btnC);
		
		actionRecived = new JLabel(""); // making global variables
		actionRecived.setBounds(10, 270, 78, 26);
		frame.getContentPane().add(actionRecived);
	}
	
	private JLabel actionRecived;
}
